<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

function fn_create_staff($fields)
{
    return db_query('INSERT INTO ?:staff ?e', $fields);
}

function fn_get_staff($id)
{
    return db_get_row('SELECT * FROM ?:staff WHERE staff_id = ?i', $id);
}

function fn_update_staff($fields, $id)
{
    return db_query('UPDATE ?:staff SET ?u WHERE staff_id = ?i', $fields, $id);
}

function fn_delete_staff($id)
{
    return db_query('DELETE FROM ?:staff WHERE staff_id = ?i', $id);
}

function fn_count_staff()
{
    return db_get_field('SELECT COUNT(*) FROM ?:staff');
}

function fn_get_staff_list($page, $items_per_page)
{
    $limit = db_quote('LIMIT ?i, ?i', ($page - 1) * $items_per_page, $items_per_page);
    return db_get_hash_array('SELECT * FROM ?:staff ?p', 'staff_id', $limit);
}

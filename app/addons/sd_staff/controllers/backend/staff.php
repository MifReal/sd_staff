<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if ($mode == 'delete') {
    fn_delete_staff($_REQUEST['staff_id']);
    return array(CONTROLLER_STATUS_OK, 'staff.manage');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'm_update') {
        foreach ($_REQUEST['staff_data'] as $staff_id => $fields) {
            fn_update_staff($fields, $staff_id);
        }
    } elseif ($mode == 'update') {
        if (empty($_REQUEST['staff_id'])) {
            fn_create_staff($_REQUEST['fields']);
        } else {
            fn_update_staff($_REQUEST['fields'], $_REQUEST['staff_id']);
        }
    }

    return array(CONTROLLER_STATUS_OK, 'staff.manage');
}

if ($mode == 'update') {
    $fields = empty($_REQUEST['staff_id']) ? array() : fn_get_staff($_REQUEST['staff_id']);
    Tygh::$app['view']->assign('fields', $fields);
} else {
    $page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
    $items_per_page = empty($_REQUEST['items_per_page']) ? Registry::get('settings.Appearance.admin_elements_per_page') : $_REQUEST['items_per_page'];
    Tygh::$app['view']->assign('staff', fn_get_staff_list($page, $items_per_page));
    Tygh::$app['view']->assign('search', array(
        'page' => $page,
        'items_per_page' => $items_per_page,
        'total_items' => fn_count_staff()
    ));
}

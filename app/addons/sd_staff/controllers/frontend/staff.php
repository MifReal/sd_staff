<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

$staff = db_get_hash_array("SELECT staff_id, first_name, last_name, title, email, description FROM ?:staff", 'staff_id');

if ($mode == 'view') {
    $fields = empty($_REQUEST['staff_id']) || empty($staff[$_REQUEST['staff_id']]) ? array() : $staff[$_REQUEST['staff_id']];
    Tygh::$app['view']->assign('fields', $fields);
} else {
    Tygh::$app['view']->assign('staff', $staff);
}

{if $fields}
    {assign var="id" value=$fields.staff_id}
{else}
    {assign var="id" value=0}
{/if}

{capture name="mainbox"}
<form action="{""|fn_url}" method="post" name="staff_update_form" class="form-horizontal form-edit" enctype="multipart/form-data">
    <input type="hidden" name="staff_id" value="{$id}" />
    {include file="addons/sd_staff/views/staff/components/fields.tpl"}

    {capture name="buttons"}
        {if $id}
            {include file="buttons/delete_profile.tpl" but_href="staff.delete?staff_id=$id" but_role="action"}
            {include file="buttons/save_cancel.tpl" but_role="submit-link" but_name="dispatch[staff.`$runtime.mode`]" but_target_form="staff_update_form" save=$id}
        {else}
            {include file="buttons/button.tpl" but_text=__("create") but_role="submit-link" but_name="dispatch[staff.`$runtime.mode`]" but_target_form="staff_update_form"}
        {/if}
    {/capture}
</form>
{/capture}

{include file="common/mainbox.tpl" title=__("staff") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}
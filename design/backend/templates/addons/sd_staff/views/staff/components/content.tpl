<table width="100%" class="table table-middle table-responsive">
<thead>
<tr>
    <th class="left mobile-hide" width="10%">{__("position_short")}</th>
    <th width="80%" class="left">{__("first_name_and_last_name")}</th>
    <th width="10%" class="right">{__("status")}</th>
</tr>
</thead>
<tbody>
{foreach from=$staff item="member"}
    <tr class="cm-row-status-{$member.status|lower}">
        <td width="10%" class="mobile-hide" data-th="{__("position_short")}">
            <input type="text" name="staff_data[{$member.staff_id}][position]" size="3" maxlength="10" value="{$member.position}" class="input-micro input-hidden" />
        </td>
        <td class="row-status" width="80%" data-th="{__("first_name_and_last_name")}">
            <a href="{"staff.update?staff_id=`$member.staff_id`"|fn_url}">{$member.first_name} {$member.last_name}</a>
        </td>
        <td width="10%" class="nowrap right" data-th="{__("status")}">
            {include file="common/select_popup.tpl" id=$member.staff_id status=$member.status object_id_name="staff_id" table="staff" popup_additional_class="dropleft"}
        </td>
    </tr>
{/foreach}
</tbody>
</table>
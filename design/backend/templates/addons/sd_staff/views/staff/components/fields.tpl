<div class="control-group">
    <label for="first_name" class="control-label">{__("first_name")}:</label>
    <div class="controls">
        <input type="text" id="first_name" name="fields[first_name]" class="input-large" size="32" maxlength="128" value="{$fields.first_name}" />
    </div>
</div>

<div class="control-group">
    <label for="last_name" class="control-label">{__("last_name")}:</label>
    <div class="controls">
        <input type="text" id="last_name" name="fields[last_name]" class="input-large" size="32" maxlength="128" value="{$fields.last_name}" />
    </div>
</div>

<div class="control-group">
    <label for="title" class="control-label">{__("function_title")}:</label>
    <div class="controls">
        <input type="text" id="title" name="fields[title]" class="input-large" size="32" maxlength="128" value="{$fields.title}" />
    </div>
</div>

<div class="control-group">
    <label for="email" class="control-label cm-email">{__("email")}:</label>
    <div class="controls">
        <input type="text" id="email" name="fields[email]" class="input-large" size="32" maxlength="128" value="{$fields.email}" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="elm_staff_descr">{__("description")}:</label>
    <div class="controls">
        <textarea id="elm_staff_descr" name="fields[description]" cols="55" rows="8" class="cm-wysiwyg input-large">{$fields.description}</textarea>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="elm_staff_position">{__("position")}</label>
    <div class="controls">
        <input type="text" size="3" name="fields[position]" value="{if $fields.position}{$fields.position}{else}0{/if}" class="input-medium" id="elm_staff_position" />
    </div>
</div>
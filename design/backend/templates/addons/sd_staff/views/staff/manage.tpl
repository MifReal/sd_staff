{capture name="mainbox"}
<form action="{""|fn_url}" method="post" name="staff_manage_form" id="staff_manage_form">
<input type="hidden" name="redirect_url" value="{$config.current_url}" />
{include file="common/pagination.tpl"}
<div class="items-container">
    {include file="addons/sd_staff/views/staff/components/content.tpl"}
</div>
{include file="common/pagination.tpl"}

{capture name="buttons"}
    {include file="buttons/save.tpl" but_name="dispatch[staff.m_update]" but_role="action" but_target_form="staff_manage_form" but_meta="cm-submit"}
{/capture}

{capture name="adv_buttons"}
    {include file="common/tools.tpl" tool_href="staff.update" title=__("add_staff") icon="icon-plus"}
{/capture}

</form>
{/capture}


{include file="common/mainbox.tpl" title=__("staff") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons}
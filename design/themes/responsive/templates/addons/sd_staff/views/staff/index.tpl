<table width="100%" class="ty-table">
<thead>
    <th>{__("first_name_and_last_name")}</th>
    <th>{__("function_title")}</th>
</thead>
<tbody>
{foreach from=$staff item="member"}
    <tr>
        <td><a href="{"staff.view?staff_id=`$member.staff_id`"|fn_url}">{$member.first_name} {$member.last_name}</a></td>
        <td>{$member.title}</td>
    </tr>
{/foreach}
</tbody>
</table>
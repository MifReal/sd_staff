<table width="100%" class="ty-table">
<thead>
    <th>{__("first_name")}</th>
    <th>{__("last_name")}</th>
    <th>{__("function_title")}</th>
    <th>{__("email")}</th>
    <th>{__("description")}</th>
</thead>
<tbody>
    <tr>
        <td>{$fields.first_name}</td>
        <td>{$fields.last_name}</td>
        <td>{$fields.title}</td>
        <td>{$fields.email}</td>
        <td>{$fields.description}</td>
    </tr>
</tbody>
</table>